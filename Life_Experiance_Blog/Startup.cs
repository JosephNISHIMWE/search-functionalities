﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Life_Experiance_Blog.Startup))]
namespace Life_Experiance_Blog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
