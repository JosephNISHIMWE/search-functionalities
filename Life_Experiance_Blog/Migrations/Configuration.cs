namespace Life_Experiance_Blog.Migrations
{
    using Life_Experiance_Blog.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;


    internal sealed class Configuration : DbMigrationsConfiguration<Life_Experiance_Blog.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Life_Experiance_Blog.Models.ApplicationDbContext context)
        {
            var dbContext = new ApplicationDbContext();
            var userStore = new UserStore<ApplicationUser>(dbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            ApplicationUser user = new ApplicationUser()
            {
                UserName = "Amin.Admin",
                Email = "blog_manager@leb.com",
                FirstName="Amin",
                LastName = "Alkhatib"
            };

            userManager.Create(user, "@Amin1234");

            var roleStore = new RoleStore<IdentityRole>(dbContext);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            roleManager.Create(new IdentityRole("Admin"));
            ApplicationUser dbUser = userManager.FindByEmail("blog_manager@leb.com");

            userManager.AddToRole(dbUser.Id, "Admin");

        }
    }
}
