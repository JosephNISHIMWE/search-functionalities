namespace Life_Experiance_Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Save : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Blogs", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Blogs", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            AlterColumn("dbo.Blogs", "Body", c => c.String());
            AlterColumn("dbo.Blogs", "Title", c => c.String());
        }
    }
}
