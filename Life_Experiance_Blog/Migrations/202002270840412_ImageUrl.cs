namespace Life_Experiance_Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImageUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "ImageUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Blogs", "ImageUrl");
        }
    }
}
