﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Life_Experiance_Blog.ViewModels
{
    public class BlogersViewModel
    {
        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Id { get; set; }
    }
}