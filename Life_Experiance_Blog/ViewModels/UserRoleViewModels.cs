﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Life_Experiance_Blog.Models
{
    public class UserRoleViewModels
    {
        public List<String> Users { get; set; }
        public List<String> Roles { get; set; }
    }
}