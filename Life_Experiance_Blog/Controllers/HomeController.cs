﻿using Life_Experiance_Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Life_Experiance_Blog.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {          

            return View(db.Blogs.ToList());
        }

        public ActionResult Blogers()
        {
            var Blogers = (from b in db.Blogs
                           select new ViewModels.BlogersViewModel
                           {
                               FirstName = b.Author.FirstName,
                               LastName = b.Author.LastName,
                               Id = b.Author.Id

                           }).ToList();
            return View(Blogers);
        }

        public ActionResult BlogersDetails(string id)
        {
            var User = (from u in db.Users
                        where u.Id == id
                        select u).SingleOrDefault();
            return View(User);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}