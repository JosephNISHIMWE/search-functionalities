﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Life_Experiance_Blog.Models;

namespace Life_Experiance_Blog.Controllers
{
    public class PostTagsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: PostTags
        public ActionResult Index()
        {
            var postTags = db.PostTags.Include(p => p.Post).Include(p => p.Tag);
            return View(postTags.ToList());
        }

        // GET: PostTags/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostTag postTag = db.PostTags.Find(id);
            if (postTag == null)
            {
                return HttpNotFound();
            }
            return View(postTag);
        }

        // GET: PostTags/Create
        public ActionResult Create()
        {
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name");
            return View();
        }

        // POST: PostTags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TagId,PostId")] PostTag postTag)
        {
            if (ModelState.IsValid)
            {
                db.PostTags.Add(postTag);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", postTag.PostId);
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", postTag.TagId);
            return View(postTag);
        }

        // GET: PostTags/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostTag postTag = db.PostTags.Find(id);
            if (postTag == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", postTag.PostId);
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", postTag.TagId);
            return View(postTag);
        }

        // POST: PostTags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TagId,PostId")] PostTag postTag)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postTag).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", postTag.PostId);
            ViewBag.TagId = new SelectList(db.Tags, "Id", "Name", postTag.TagId);
            return View(postTag);
        }

        // GET: PostTags/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostTag postTag = db.PostTags.Find(id);
            if (postTag == null)
            {
                return HttpNotFound();
            }
            return View(postTag);
        }

        // POST: PostTags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PostTag postTag = db.PostTags.Find(id);
            db.PostTags.Remove(postTag);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
