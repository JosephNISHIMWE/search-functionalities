﻿using Life_Experiance_Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Life_Experiance_Blog.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(string Searching)
        {
            var Blog = from s in db.Blogs
                       select s;
            if(!String.IsNullOrEmpty(Searching))
            {
                Blog = Blog.Where(s => s.Title.Contains(Searching));
                //Blog = Blog.Where(s => s.Body.Contains(Searching));
            }
            return View(Blog.ToList());
        }
    }
}