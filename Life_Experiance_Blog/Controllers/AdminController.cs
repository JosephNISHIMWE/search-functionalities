﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Life_Experiance_Blog.Models;

namespace Life_Experiance_Blog.Controllers
{
    [Authorize(Roles = "Admin, HR")]
    public class AdminController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Admin

        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddUser()
        {
            //return View();
            return RedirectToAction("Register","Account");
        }


        public ActionResult CreateRole()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CreateRole(string roleName)
        {
            var rStore = new RoleStore<IdentityRole>(db);
            var rManager = new RoleManager<IdentityRole>(rStore);

            var role = rManager.FindByName(roleName);
            if (role == null)
            {
                role = new IdentityRole(roleName);
                rManager.Create(role);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult AssignRole()
        {
            var roles = db.Roles.Select(r => r.Name).ToList(); ;
            var users = db.Users.Select(u => u.Email).ToList();
            UserRoleViewModels obj = new UserRoleViewModels();
            obj.Roles = roles;
            obj.Users = users;
            return View(obj);
        }
        [HttpPost]
        public ActionResult AssignRole(string User, string Role)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var uStore = new UserStore<ApplicationUser>(db);
            var uManager = new UserManager<ApplicationUser>(uStore);
            var user = uManager.FindByEmail(User);
            //var role =rManager.FindByName(Role);
            if (user != null)
            {
                uManager.AddToRole(user.Id, Role);
            }
            return RedirectToAction("Index");
        }
    }
}