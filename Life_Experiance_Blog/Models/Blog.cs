﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Life_Experiance_Blog.Models
{
    public class Blog
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Body { get; set; }

        [DataType(DataType.Date)]
        public DateTime Created { get; set; }

        [Display(Name = "Blog Poster")]
        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
        public virtual ApplicationUser Author { get; set; }
        
    }
}