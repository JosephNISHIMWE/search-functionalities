﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Life_Experiance_Blog.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}