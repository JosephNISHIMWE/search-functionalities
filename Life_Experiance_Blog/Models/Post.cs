﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Life_Experiance_Blog.Models
{
    public class Post
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }

        public int Views { get; set; }

        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}