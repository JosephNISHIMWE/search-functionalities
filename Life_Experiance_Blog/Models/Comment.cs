﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Life_Experiance_Blog.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [Required]
        [StringLength(2000)]
        public string Body { get; set; }

        public DateTime Created { get; set; }

        public int PostId { get; set; }

        public virtual ApplicationUser Author { get; set; }

        public virtual Post Post { get; set; }
    }
}